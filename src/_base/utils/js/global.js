jQuery.extend({
  global: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.blocked = false;

        _private.loadActions();

        // jQuery(_private.holder).on('click', function(event) {
        //     _private.clickListener();
        // });

        jQuery(_private.holder).on('keyup', function(event) {
            _private.keyListener(event);
        });

        // jQuery(_private.holder).on('mousewheel', function(event) {
        //     _private.scrollListener(event);
        // });
      },

      loadActions: function() {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 0);
        sessionStorage.clear();
      },

      clickListener: function() {
        //
      },

      keyListener: function(event) {
        var next = '';
        var previous = '';
        var storeNav = sessionStorage.currentItem;

        if (storeNav === 'introduction') {
          previous = false;
          next = storeNav + '__nav.nav--next';
        } else if (storeNav === 'closing') {
          previous = storeNav + '__nav.nav--previous';
          next = false;
        } else {
          previous = storeNav + '__nav.nav--previous';
          next = storeNav + '__nav.nav--next';
        }

        if(event.key === 'ArrowUp' || event.key === 'w' || event.key === 'ArrowRight' || event.key === 'a') {
          if (previous != false) {
            jQuery(previous).click();
          }
        }

        if(event.key === 'ArrowDown' || event.key === 's' || event.key === 'ArrowLeft' || event.key === 'd') {
          if (next != false) {
            jQuery(next).click();
          }
        }
        // TODO: maybe esc later
      },

      scrollListener: function(event) {
        var next = '';
        var previous = '';
        var storeNav = sessionStorage.currentItem; // maybe this caches too much, we'll test
        // _private.blocked = false;

        if (storeNav === 'introduction') {
          previous = false;
          next = storeNav + '__nav.nav--next';
        } else if (storeNav === 'closing') {
          previous = storeNav + '__nav.nav--previous';
          next = false;
        } else {
          previous = storeNav + '__nav.nav--previous';
          next = storeNav + '__nav.nav--next';
        }

        if (_private.blocked === false) {
          if (event.originalEvent.wheelDelta >= 0) {
            _private.blocked = true;
            jQuery(previous).click();
          } else {
            _private.blocked = true;
            jQuery(next).click();
          }
        }

        setTimeout(function()
        {
          _private.blocked = false;
        }, 1000);
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('body').each(function(_index, _ell) {
      var global = new jQuery.global(jQuery(_ell));
    });
  });
});

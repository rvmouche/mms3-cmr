jQuery.extend({
  introduction: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.button = jQuery(_private.holder).find('.introduction__section__buttons__button');

        jQuery(_private.button).on('click', function(event) {
            _private.clickListener(event);
        });
      },
      clickListener: function(event) {
        if(jQuery(event.target).attr('data-modal') == 'agenda-standpunten') {
          jQuery.localModal({'category': 'agenda', 'name': 'standpunten', 'extension': 'php'});
        }
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.introduction__section').each(function(_index, _ell) {
      var introduction = new jQuery.introduction(jQuery(_ell));
    });
  });
});

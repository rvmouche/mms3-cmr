<section class="section section--video introduction__section" data-menu="introduction" data-menu-name="Introductie" data-time-start="9:30" data-time-end="10:00">
  <button class="play-button" data-start="introduction-video" data-endposter="assets/poster-introductie-post.jpg"></button>
  <video
    id="introduction-video"
    class="video-js introduction__video"
    controls
    autoplay
    preload="auto"
    poster="assets/poster-introductie.jpg"
    data-setup='{}'>
  <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/introductie.mp4" type="video/mp4"></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="http://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
  </video>
  <div class="introduction__section__buttons after-play-buttons">
    <button class="after-play-buttons__button introduction__section__buttons__button" data-modal="agenda-standpunten">Standpunten</button>
  </div>
</section>
<div class="nav nav--next introduction__nav" data-link=".definition__nav"></div>

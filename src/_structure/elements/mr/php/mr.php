<div class="nav nav--previous mr__nav" data-link=".definition__nav"></div>
<section class="section section--video mr__section" data-menu="mr" data-menu-name="Medezeggenschap" data-time-start="12:00" data-time-end="14:00">
  <button class="play-button" data-start="mr-video" xdata-endposter="assets/poster-mr-post.jpg"></button>
  <video
    id="mr-video"
    class="video-js mr__video"
    controls
    preload="auto"
    poster="assets/poster-mr.jpg"
    data-setup='{}'>
  <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/mr.mp4" type="video/mp4"></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="http://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>
<div class="mr__section__buttons after-play-buttons">
  <button class="after-play-buttons__button mr__section__buttons__button" data-modal="agenda-landelijk">Landelijk</button>
  <a href="https://www.zuydnet.nl/studeren/studieloopbaan/bindend-afwijzend-studieadvies-bas" target="_blank">
    <button class="after-play-buttons__button after-play-buttons__button--zuyd mr__section__buttons__button mr__section__buttons__button--zuyd">BAS <img src="assets/zuyd.png" alt="Zuydnet"></button>
  </a>
</div>
</section>
<div class="nav nav--next mr__nav" data-link=".agenda__nav"></div>

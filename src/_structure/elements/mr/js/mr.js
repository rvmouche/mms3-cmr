jQuery.extend({
  medezeggenschap: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.button = jQuery(_private.holder).find('.mr__section__buttons__button');

        jQuery(_private.button).on('click', function(event) {
            _private.clickListener(event);
        });
      },
      clickListener: function(event) {
        if(jQuery(event.target).attr('data-modal') == 'agenda-landelijk') {
          jQuery.localModal({'category': 'agenda', 'name': 'landelijk', 'extension': 'php'});
        }
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.mr__section').each(function(_index, _ell) {
      var medezeggenschap = new jQuery.medezeggenschap(jQuery(_ell));
    });
  });
});

<div class="nav nav--previous closing__nav" data-link=".whoiswho__nav"></div>
<section class="section section--video closing__section" data-menu="closing" data-menu-name="Sfeer" data-menu-name="Introductie" data-time-start="18:00" data-time-end="21:30">
  <button class="play-button" data-start="closing-video"></button>
  <video
    id="closing-video"
    class="video-js closing__video"
    controls
    preload="auto"
    poster="assets/poster-closing.jpg"
    data-setup='{}'>
  <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/closing.mp4" type="video/mp4"></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="http://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>
<div class="closing__section__buttons after-play-buttons">
  <a href="mailto:raimond.vanmouche@zuyd.nl">
    <button class="after-play-buttons__button closing__section__buttons__button">Contact</button>
  </a>
</div>
</section>

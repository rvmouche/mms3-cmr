<div class="nav nav--previous whoiswho__nav" data-link=".agenda__nav"></div>
<section class="section whoiswho__section" data-menu="whoiswho" data-menu-name="Leden?" data-menu-name="Introductie" data-time-start="17:00">
  <img src="assets/groepsfoto.jpg" usemap="#image-map">

  <map name="image-map">
      <!-- <area  alt="ruben" title="ruben" href="#" coords="432,631,433,569,407,533,415,480,443,418,502,406,508,315,555,314,558,404,621,431,618,508,559,532,529,630" shape="poly"> -->
      <!-- <area  alt="katja" title="katja" href="#" coords="407,475,383,361,419,348,430,253,467,250,488,332,492,402,441,413" shape="poly"> -->
      <area  alt="daan" title="daan" href="#" coords="489,322,477,240,451,244,466,197,498,189,509,118,561,121,555,189,598,217,554,233,546,302" shape="poly">
      <!-- <area  alt="hans-s" title="hans-s" href="#" coords="562,189,566,111,558,45,608,46,606,107,614,148,602,171,586,209" shape="poly"> -->
      <area  alt="laura" title="laura" href="#" coords="554,309,558,238,615,232,631,305,586,407,563,396" shape="poly">
      <!-- <area  alt="shakila" title="shakila" href="#" coords="591,415,631,317,668,315,714,396,663,420" shape="poly"> -->
      <!-- <area  alt="jof" title="jof" href="#" coords="631,283,614,224,595,211,616,152,617,91,668,89,663,148,680,176,686,203,682,224,652,232" shape="poly"> -->
      <!-- <area  alt="marc" title="marc" href="#" coords="635,299,655,238,697,229,684,135,745,142,745,193,716,244,684,323" shape="poly"> -->
      <!-- <area  alt="angelique" title="angelique" href="#" coords="717,389,684,338,744,204,770,204,823,322,796,324,778,291,726,299" shape="poly"> -->
      <!-- <area  alt="norbert" title="norbert" href="#" coords="825,306,763,183,807,136,796,61,886,52,872,144,909,183,833,216" shape="poly"> -->
      <!-- <area  alt="anouk" title="anouk" href="#" coords="898,313,900,204,942,205,966,269,995,302,970,355" shape="poly"> -->
      <!-- <area  alt="jose" title="jose" href="#" coords="979,268,910,167,928,119,992,136" shape="poly"> -->
      <!-- <area  alt="mattias" title="mattias" href="#" coords="972,429,980,358,1005,331,988,234,1079,226,1062,303,1119,326,1105,634,1038,633,1034,506" shape="poly"> -->
      <area  alt="benjamin" title="benjamin" href="#" coords="893,319,872,424,844,437,788,393,798,340,836,322,837,229,883,224" shape="poly">
      <!-- <area  alt="han-w" title="han-w" href="#" coords="802,634,795,551,836,445,875,430,900,336,941,346,954,424,996,469,1028,558,1016,628" shape="poly"> -->
      <!-- <area  alt="werner" title="werner" href="#" coords="697,520,674,425,720,404,729,309,784,303,782,392,828,442,795,626,754,633,745,572" shape="poly"> -->
      <area  alt="raimond" title="raimond" href="#" coords="537,630,567,536,612,527,625,430,669,428,684,510,730,564,745,634" shape="poly">
  </map>
</section>
<div class="nav nav--next whoiswho__nav" data-link=".closing__nav"></div>

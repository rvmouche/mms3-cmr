jQuery.extend({
  whoiswho: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.imagemap = jQuery(_private.holder).find('map');

        _private.makeResponsive();

        jQuery(_private.imagemap).on('click', function(event) {
            _private.clickListener(event);
        });
      },
      makeResponsive: function() {
        jQuery(".whoiswho__section map").imageMapResize();
      },
      clickListener: function(event) {
        event.preventDefault();
        jQuery.slideOver({'category': 'whoiswho', 'name': jQuery(event.target).attr('title'), 'extension': 'php'});
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.whoiswho__section').each(function(_index, _ell) {
      var whoiswho = new jQuery.whoiswho(jQuery(_ell));
    });
  });
});

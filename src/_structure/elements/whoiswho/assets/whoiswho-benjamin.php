<div class="slideover slideover--whoiswho slideover--right">
  <div class="slideover__header">
    <div class="slideover__header__close"></div>
    <div class="slideover__header__square">
      <button class="native-play native-play--slideover whoiswho_benjamin" onclick="jQuery.nativePlay({'id': 'whoiswho_benjamin', 'action': 'play', 'hide': '.whoiswho_benjamin'});"></button>
      <video
        id="whoiswho_benjamin"
        class="slideover__video"
        preload="auto"
        poster="assets/poster-whoiswho-benjamin.jpg">
        <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/whoiswho-benjamin.mp4" type="video/mp4"></source>
        Your browser does not support the html5-video tag.
      </video>
    </div>
    <h2 class="slideover__header__name">
      Benjamin Velge
    </h2>
  </div>
  <div class="slideover__content">
    Ik ben Benjamin Velge, student HBO-ICT. Als vicevoorzitter van de CMR probeer ik zo goed mogelijk alle belangen van de studenten te behartigen als mogelijk. Hiermee ben ik dan ook ongeveer 2 dagen in de week mee bezig.
    <br>
    <br>
    Als actief verenigingslid probeer ik tevens op vele andere manieren betrokken te blijven op Zuyd en daardoor met vele studenten kan spreken over Zuyd.

  </div>
  <div class="slideover__footer">
    <a href="mailto:benjamin.velge@zuyd.nl" class="slideover__footer__button">Email</a>
    <a class="slideover__footer__button" onclick="jQuery.localModal({'category': 'agenda', 'name': 'standpunten', 'extension': 'php'});">Standpunten</a>
  </div>
</div>

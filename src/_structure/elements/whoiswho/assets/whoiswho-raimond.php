<div class="slideover slideover--whoiswho slideover--right">
  <div class="slideover__header">
    <div class="slideover__header__close"></div>
    <div class="slideover__header__square">
      <button class="native-play native-play--slideover whoiswho_raimond" onclick="jQuery.nativePlay({'id': 'whoiswho_raimond', 'action': 'play', 'hide': '.whoiswho_raimond'});"></button>
      <video
        id="whoiswho_raimond"
        class="slideover__video"
        preload="auto"
        poster="assets/poster-whoiswho-raimond.jpg">
        <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/whoiswho-raimond.mp4" type="video/mp4"></source>
        Your browser does not support the html5-video tag.
      </video>
    </div>
    <h2 class="slideover__header__name">
      Raimond van Mouche
    </h2>
  </div>
  <div class="slideover__content">
    Raimond is student Communication and Multimedia Design in Maastricht. Hij zit in de CMR om communicatie en studentenbetrokkenheid te verbeteren, maar ook gewoon omdat hij het leuk vindt. Naast lid van de CMR is Raimond vice-voorzitter van de Deelraad van de Faculteit van de Kunsten en lid van de Opleidingscommissie van zijn academie.
  <div class="slideover__footer">
    <a href="mailto:raimond.vanmouche@zuyd.nl" class="slideover__footer__button">Email</a>
    <a href="tel:+31611929155" class="slideover__footer__button">Telefoon</a>
  </div>
</div>

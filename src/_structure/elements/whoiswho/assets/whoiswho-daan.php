<div class="slideover slideover--whoiswho slideover--right">
  <div class="slideover__header">
    <div class="slideover__header__close"></div>
    <div class="slideover__header__square" style="background: url(assets/whoiswho-daan.jpg); background-size: cover; background-position: center center;"></div>
    <h2 class="slideover__header__name">
      Daan Cruijsen
    </h2>
  </div>
  <div class="slideover__content">
    Mijn naam is Daan Cruijsen en ik studeer fysiotherapie aan ZUYD in Heerlen. Toen ik de aanmelding voor de CMR zag heb ik mijzelf meteen aangemeld omdat ik graag iets wil betekenen voor mijn medestudenten en erg benieuwd was naar hoe het er organisatief aan toe gaat binnen de hogeschool.
  <div class="slideover__footer">
    <a href="mailto:daan.cruijsen@zuyd.nl" class="slideover__footer__button">Email</a>
  </div>
</div>

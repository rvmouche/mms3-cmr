<div class="slideover slideover--whoiswho slideover--right">
  <div class="slideover__header">
    <div class="slideover__header__close"></div>
    <div class="slideover__header__square" style="background: url(assets/whoiswho-laura.jpg); background-size: cover; background-position: center center;"></div>
    <h2 class="slideover__header__name">
      Laura Raijmann
    </h2>
  </div>
  <div class="slideover__content">
    Laura is student aan de Hotel Management School Maastricht. Ze zet zich in voor alle studenten en medewerkers. Zie haar dan ook zeker als aanspreekpunt! Ze is tevens lid van de medezeggenschapsraad Facility Management/Hotel Management School Maastricht.
  <div class="slideover__footer">
    <a href="mailto:laura.raijmann@zuyd.nl" class="slideover__footer__button">Email</a>
  </div>
</div>

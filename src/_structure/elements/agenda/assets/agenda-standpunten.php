<div class="localmodal localmodal--agenda">
  <div class="localmodal__header" id="localmodal__header">
    <div class="localmodal__header__close"></div>
    <h2 class="localmodal__header__name localmodal__header__name--standpunten">
      Standpunten
    </h2>
  </div>
  <div class="localmodal__content agenda__standpunten">
    <button class="native-play agenda-standpunten agenda-standpunt-1" onclick="jQuery.nativePlay({'id': 'agenda-standpunt-1', 'action': 'play', 'hide': '.agenda-standpunt-1'});"></button>
    <video
      id="agenda-standpunt-1"
      class="agenda__standpunten__video"
      preload="auto"
      poster="assets/poster-agenda-standpunten-roostering.jpg">
      <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/standpunten-roostering.mp4" type="video/mp4"></source>
      Your browser does not support the html5-video tag.
    </video>

    <button class="native-play agenda-standpunten agenda-standpunt-2" onclick="jQuery.nativePlay({'id': 'agenda-standpunt-2', 'action': 'play', 'hide': '.agenda-standpunt-2'});"></button>
    <video
      id="agenda-standpunt-2"
      class="agenda__standpunten__video"
      preload="auto"
      poster="assets/poster-agenda-standpunten-inschrijving.jpg">
      <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/standpunten-inschrijving.mp4" type="video/mp4"></source>
      Your browser does not support the html5-video tag.
    </video>

    <button class="native-play agenda-standpunten agenda-standpunt-3" onclick="jQuery.nativePlay({'id': 'agenda-standpunt-3', 'action': 'play', 'hide': '.agenda-standpunt-3'});"></button>
    <video
      id="agenda-standpunt-3"
      class="agenda__standpunten__video"
      preload="auto"
      poster="assets/poster-agenda-standpunten-betrokkenheid.jpg">
      <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/standpunten-betrokkenheid.mp4" type="video/mp4"></source>
      Your browser does not support the html5-video tag.
    </video>

    <a href="https://studentenbelangzuyd.nl" class="agenda__standpunten--image" target="_blank">
      <img src="assets/agenda-standpunten-sbz.png" alt="Studentenbelang Zuyd">
    </a>
  </div>
</div>

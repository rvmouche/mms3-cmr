<div class="localmodal localmodal--agenda">
  <div class="localmodal__header" id="localmodal__header">
    <div class="localmodal__header__close"></div>
    <h2 class="localmodal__header__name localmodal__header__name--landelijk">
      Landelijke samenwerking
    </h2>
  </div>
  <div class="localmodal__content agenda__landelijk">
    <div class="agenda__landelijk__lsvb">
      <img src="assets/agenda-landelijk-lsvb.png" alt="Het LSVB zet zich als studentenvakbond in voor de belangen van studenten. Bij het LSVB zit ook het SOM - de belangenorganisatie voor medezeggenschap op HBO.">
      <a href="http://lsvb.nl" target="_blank">
        <button class="agenda__landelijk__button agenda__landelijk__button--lsvb">LSVB&nbsp;&nbsp;&nbsp; <img src="assets/link.svg"></button>
      </a>
    </div>
    <div class="agenda__landelijk__iso">
      <img src="assets/agenda-landelijk-iso.png" alt="ISO staat voor Interstedelijk Studenten Overleg en is een studentenorganisatie die bestaat uit CMR'en en fracties - ze zetten zich in vanuit de medezeggenschap.">
      <a href="http://iso.nl" target="_blank">
        <button class="agenda__landelijk__button agenda__landelijk__button--iso">ISO <img src="assets/link.svg"></button>
      </a>
    </div>
  </div>
</div>

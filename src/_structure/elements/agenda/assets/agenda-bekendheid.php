<div class="localmodal localmodal--agenda">
  <div class="localmodal__header" id="localmodal__header">
    <div class="localmodal__header__close"></div>
    <h2 class="localmodal__header__name localmodal__header__name--bekendheid">
      Bekendheid
    </h2>
  </div>
  <div class="localmodal__content agenda__bekendheid">
    <button class="native-play agenda-bekendheid agenda-bekendheid-1" onclick="jQuery.nativePlay({'id': 'agenda-bekendheid-1', 'action': 'play', 'hide': '.agenda-bekendheid-1'});"></button>
    <video
      id="agenda-bekendheid-1"
      class="agenda__bekendheid__video"
      preload="auto"
      poster="assets/poster-agenda-bekendheid.jpg">
      <source src="https://rvm-mms2.s3.amazonaws.com/mms3-cmr/bekendheid.mp4" type="video/mp4"></source>
      Your browser does not support the html5-video tag.
    </video>

    <div class="agenda__bekendheid--image first">
      <img src="assets/agenda-bekendheid-verkiezingen.jpg" alt="Heftige verkiezingen">
    </div>

    <div class="agenda__bekendheid--image second">
      <img src="assets/agenda-bekendheid-moodle.jpg" alt="Moodle / Blackboard">
    </div>

    <div class="agenda__bekendheid--image third">
      <img src="assets/agenda-bekendheid-gesprek.jpg" alt="Persoonlijk gesprek">
    </div>
  </div>
</div>

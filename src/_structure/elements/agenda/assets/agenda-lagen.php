<div class="localmodal localmodal--agenda">
  <div class="localmodal__header" id="localmodal__header">
    <div class="localmodal__header__close"></div>
    <h2 class="localmodal__header__name localmodal__header__name--lagen">
      Faculteiten en opleidingen
    </h2>
  </div>
  <div class="localmodal__content agenda__lagen">
    <div class="agenda__lagen__cmr">
      <a href="https://www.zuydnet.nl/over-zuyd/medezeggenschap/medezeggenschapraad/centrale-medezeggenschapsraad" target="_blank">
        <button class="agenda__lagen__button agenda__lagen__button--cmr">meer... <img src="assets/zuyd.png" alt="Zuydnet"></button>
      </a>
    </div>
    <div class="agenda__lagen__dmr">
      <a href="https://www.zuydnet.nl/over-zuyd/medezeggenschap/medezeggenschapraad/deelraden" target="_blank">
        <button class="agenda__lagen__button agenda__lagen__button--dmr">DMR <img src="assets/zuyd.png" alt="Zuydnet"></button>
      </a>
    </div>
    <div class="agenda__lagen__oc">
      <a href="https://www.zuydnet.nl/over-zuyd/medezeggenschap/opleidingscommissie" target="_blank">
        <button class="agenda__lagen__button agenda__lagen__button--oc">OC <img src="assets/zuyd.png" alt="Zuydnet"></button>
      </a>
    </div>
  </div>
</div>

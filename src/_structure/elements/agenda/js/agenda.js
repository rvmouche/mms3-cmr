jQuery.extend({
  agenda: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.item = jQuery(_private.holder).find('.agenda__section__left__item');
        _private.preview = jQuery(_private.holder).find('.agenda__section__right');
        _private.fullscreen = jQuery(_private.preview).find('.agenda__section__right__fullscreenbutton');

        jQuery(_private.item).on('click', function(event) {
            _private.clickListener('item', event);
        });

        jQuery(_private.preview).on('click', function(event) {
            _private.clickListener('preview', event);
        });

        jQuery(_private.fullscreen).on('click', function(event) {
            _private.clickListener('preview', event);
        });
      },
      clickListener: function(kind, event) {
        if (kind === 'item') {
          _private.createPreview(jQuery(event.target).attr('data-preview'), jQuery(event.target).attr('data-modal'));
          jQuery(_private.holder).find('.agenda__section__left__item--active').addClass('agenda__section__left__item--done');
          jQuery(_private.holder).find('.agenda__section__left__item--active').removeClass('agenda__section__left__item--active');
          jQuery(event.target).addClass('agenda__section__left__item--active');
          jQuery(event.target).removeClass('agenda__section__left__item--done');
        }
        if (kind === 'preview') {
          if(jQuery(_private.preview).attr('data-modal') == 'agenda-standpunten') {
            jQuery.localModal({'category': 'agenda', 'name': 'standpunten', 'extension': 'php'});
          }
          if(jQuery(_private.preview).attr('data-modal') == 'agenda-bekendheid') {
            jQuery.localModal({'category': 'agenda', 'name': 'bekendheid', 'extension': 'php'});
          }
          if(jQuery(_private.preview).attr('data-modal') == 'agenda-landelijk') {
            jQuery.localModal({'category': 'agenda', 'name': 'landelijk', 'extension': 'php'});
          }
          if(jQuery(_private.preview).attr('data-modal') == 'agenda-lagen') {
            jQuery.localModal({'category': 'agenda', 'name': 'lagen', 'extension': 'php'});
          }
        }
      },
      createPreview: function(url, modal) {
        var html = "";
        jQuery(_private.preview).attr('data-modal', modal);
        if (modal === "agenda-lagen") {
          html = "<div class ='agenda__section__right__content__onethird' style='background: url(assets/agenda-lagen-preview-cmr.jpg)'></div><div class ='agenda__section__right__content__onethird midden' style='background: url(assets/agenda-lagen-preview-dmr.jpg)'></div><div class ='agenda__section__right__content__onethird' style='background: url(assets/agenda-lagen-preview-oc.jpg)'></div>'";
          jQuery(_private.preview).find('.agenda__section__right__content').html(html);
          jQuery(_private.preview).find('.agenda__section__right__content').show();
          jQuery(_private.preview).css('background', '');
        } else if (modal === "agenda-landelijk") {
          html = "<div class='agenda__section__right__content__half top' style='background: url(assets/agenda-landelijk-preview-lsvb.png)'></div><div class='agenda__section__right__content__half bottom' style='background: url(assets/agenda-landelijk-preview-iso.png)'></div>";
          jQuery(_private.preview).find('.agenda__section__right__content').html(html);
          jQuery(_private.preview).find('.agenda__section__right__content').show();
          jQuery(_private.preview).css('background', 'url(' + url + ')');
        } else if (modal === "agenda-bekendheid") {
          html = "<div class='agenda__section__right__content__half top' style='background: url(assets/poster-agenda-bekendheid.jpg)'></div><div class='agenda__section__right__content__half bottom' style='background: url(assets/agenda-betrokkenheid-preview-2.jpg)'></div>";
          jQuery(_private.preview).find('.agenda__section__right__content').html(html);
          jQuery(_private.preview).find('.agenda__section__right__content').show();
          jQuery(_private.preview).css('background', '');
        } else if (modal === "agenda-standpunten") {
          html = "<div class='agenda__section__right__content__half top' style='background: url(assets/agenda-standpunten-preview-1.jpg)'></div><div class='agenda__section__right__content__half bottom' style='background: url(assets/agenda-standpunten-preview-2.jpg)'></div>";
          jQuery(_private.preview).find('.agenda__section__right__content').html(html);
          jQuery(_private.preview).find('.agenda__section__right__content').show();
          jQuery(_private.preview).css('background', '');
        } else {
          jQuery(_private.preview).css('background', 'url(' + url + ')');
          jQuery(_private.preview).find('.agenda__section__right__content').html('');
          jQuery(_private.preview).find('.agenda__section__right__content').hide();
        }

        jQuery(_private.preview).attr('data-modal', modal);
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.agenda__section').each(function(_index, _ell) {
      var agenda = new jQuery.agenda(jQuery(_ell));
    });
    jQuery('.agenda__section__left__item:first').click();
  });
});

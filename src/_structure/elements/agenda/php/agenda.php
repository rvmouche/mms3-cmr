<div class="nav nav--previous agenda__nav" data-link=".mr__nav"></div>
<section class="section agenda__section" data-menu="agenda" data-menu-name="Agenda" data-menu-name="Introductie" data-time-start="15:00">
  <div class="agenda__section__left">
    <div class="agenda__section__left__header">
      <h2 class="agenda__section__left__header__title">
        Agenda
      </h2>
    </div>
    <div class="agenda__section__left__item agenda__section__left__item--active" data-preview="" data-modal="agenda-bekendheid">
      Bekendheid verbeteren
      <hr>
    </div>
    <div class="agenda__section__left__item" data-preview="" data-modal="agenda-lagen">
      Faculteiten en opleidingen
      <hr>
    </div>
    <div class="agenda__section__left__item" data-preview="assets/agenda-landelijk-preview.jpg" data-modal="agenda-landelijk">
      Landelijke samenwerking
      <hr>
    </div>
    <div class="agenda__section__left__item" data-preview="" data-modal="agenda-standpunten">
      Benjamin's standpunten
      <hr>
    </div>
  </div>
  <div class="agenda__section__right">
    <div class="agenda__section__right__content"></div>
    <div class="agenda__section__right__fullscreenbutton"></div>
  </div>
</section>
<div class="nav nav--next agenda__nav" data-link=".whoiswho__nav"></div>

<div class="nav nav--previous definition__nav" data-link=".introduction__section"></div>
<section class="section definition__section" data-menu="definition" data-menu-name="Wat is CMR?" data-menu-name="Introductie" data-time-start="11:00">
  <img src="assets/definition-text.png">
</section>
<div class="nav nav--next definition__nav" data-link=".mr__nav"></div>

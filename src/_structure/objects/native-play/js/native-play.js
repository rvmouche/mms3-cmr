(function( $ ) {
	$.nativePlay = function(options) {
    var args	 							= $.extend({}, options );

		var _this 							= this;

		var _private = {
			holder              	: _this,
			click									: null,


			setup: function() {
				_private.playVideo();
			},

			playVideo: function() {
				var vid = document.getElementById(args.id);

				if (args.action === 'play') {
					vid.play();
					jQuery('#' + args.id).attr('controls', true);
					jQuery(args.hide).hide();
				}

				if (args.action === 'pause') {
					vid.pause();
				}
		 },
		};

		function initialize() {
            _private.setup();
        }
        initialize();
		return this;
    };
}( jQuery ));

// Put on a slide
// jQuery.nativePlay({'id': 'myvideo', 'action': 'play', 'hide': '' });

jQuery.extend({
  navigation: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.link = jQuery(_private.holder).attr('data-link');

        jQuery(_private.holder).on('click', function(event) {
            _private.clickListener();
        });
      },

      clickListener: function(element) {
        jQuery('html, body').animate({
            scrollTop: jQuery(_private.link).offset().top
        }, 500);

        jQuery(".slideover").remove();
        jQuery('.clock').show();
        jQuery(".clock__menu").hide();

        jQuery('video').each(function() {
            this.pause();
        });

        _private.setSessionStorage(_private.link);

        if (_private.link != '.introduction__section') {
          var link = _private.link;
          var takeOut = '__nav';
          var result = link.replace(takeOut,'') + '__section';
          jQuery.localClock({'time': jQuery(result).attr('data-time-start'), 'video': true});
        } else {
          jQuery.localClock({'time': jQuery(_private.link).attr('data-time-start'), 'video': true});
        }

        if(_private.link === '.whoiswho__nav') {
          setTimeout(function()
				  {
            if (sessionStorage.whoiswho_hint != 1) {
              sessionStorage.whoiswho_hint = 1;
              jQuery.fakeCursor({'kind': 'whoiswho'});
            }
					}, 800);
        }
      },

      setSessionStorage: function(element) {
        var storeNav = element;
        var takeOut = "";
        if (storeNav  != '.introduction__section') {
          takeOut = '__nav';
          storeNav = storeNav.replace(takeOut,'');
        } else {
          takeOut = '__section';
          storeNav = storeNav.replace(takeOut,'');
        }
        sessionStorage.currentItem = storeNav;
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.nav').each(function(_index, _ell) {
      var navigation = new jQuery.navigation(jQuery(_ell));
    });
  });
});

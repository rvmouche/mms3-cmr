jQuery.extend({
  playButton: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.link = jQuery(_private.holder).attr('data-start');
        _private.endposter = jQuery(_private.holder).attr('data-endposter');
        _private.player = videojs(_private.link);
        _private.parent = jQuery(_private.holder).parent();
        _private.afterplaybuttons = jQuery(_private.parent).find('.after-play-buttons');

        jQuery(_private.holder).on('click', function(event) {
            _private.clickListener();
        });

        _private.player.on('play', function() {
            _private.buttonShower('hide');
            _private.posterSetter();
        });

        _private.player.on('ended', function() {
            _private.buttonShower('show');
        });
      },
      clickListener: function() {
        _private.player.play();
      },
      buttonShower: function(state) {
        if (state === 'hide') {
          jQuery(_private.holder).hide();
          jQuery(_private.afterplaybuttons).hide();
          if (jQuery(_private.parent).attr('data-menu') === 'introduction') {
            jQuery('.clock').hide();
          }
          jQuery(".clock__menu").hide();
        }

        if (state === 'show') {
          jQuery(_private.holder).show();
          jQuery(_private.afterplaybuttons).show();
          jQuery.localClock({'time': jQuery(_private.parent).attr('data-time-end'), 'video': true});
          jQuery('.clock').show();

          if (jQuery(_private.parent).attr('data-menu') === 'introduction') {
            setTimeout(function()
  				  {
              if (sessionStorage.introduction_hint != 1) {
                jQuery.fakeCursor({'kind': 'next'});
                sessionStorage.introduction_hint = 1;
              }
  					}, 500);
          }

          if (jQuery(_private.parent).attr('data-menu') === 'mr') {
            if (sessionStorage.mr_hint != 1) {
              jQuery.fakeCursor({'kind': 'clock'});
              sessionStorage.mr_hint = 1;
            }
          }
        }
      },
      posterSetter: function() {
        if (_private.endposter) {
          jQuery('#' + _private.link).find('.vjs-poster').css('background-image', 'url(' + _private.endposter +')');
        }
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.play-button').each(function(_index, _ell) {
      var playButton = new jQuery.playButton(jQuery(_ell));
    });
  });
});

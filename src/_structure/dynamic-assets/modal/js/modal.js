(function( $ ) {
	$.localModal = function(options) {
    var args	 							= $.extend({}, options );

		var _this 							= this;

		var _private = {
			holder              	: _this,
			click									: null,


			setup: function() {
				_private.getModal();
			},

			getModal: function() {
				jQuery(".localmodal").remove();
				$.ajax({
				 url: 'assets/' + args.category + '-' + args.name + '.' + args.extension,
			 })
			 .done(function( data ) {
				 jQuery('body').append(data);
				 jQuery('.localmodal').fadeIn(400);

				 jQuery('.localmodal__header__close').on('click', function(event) {
  					 _private.clickListener('close');
  			 });
			 });
		 },
		 clickListener: function() {
			 jQuery(".localmodal video").each(function(index, el) {
			 		jQuery.nativePlay({'id': jQuery(this).attr('id'), 'action': 'pause'});
			 });
			 jQuery(".localmodal").fadeOut(400);
		 }
		};

		function initialize() {
            _private.setup();
        }
        initialize();
		return this;
    };
}( jQuery ));

// Put on a slide
// jQuery.localModal({'category': 'agenda', 'name': 'standpunten', 'extension': 'php'});

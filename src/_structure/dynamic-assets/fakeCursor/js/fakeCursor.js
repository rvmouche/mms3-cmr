(function( $ ) {
	$.fakeCursor = function(options) {
    var args	 							= $.extend({}, options );

		var _this 							= this;

		var _private = {
			holder              	: _this,
			click									: null,


			setup: function() {
				_private.buildCursor();
			},

			buildCursor: function() {
				if (args.kind === 'next') {
					jQuery('body').append('<div class="fakecursor__next"><div class="fakecursor__next__click"></div></div>');
					jQuery('.fakecursor__next__click').show();
					jQuery('html, body').animate({
	            scrollTop: jQuery(document).scrollTop() + '50px'
	        }, 1000);
					setTimeout(function()
				  {
						jQuery('html, body').animate({
		            scrollTop: jQuery(document).scrollTop() - '50px'
		        }, 1000);
						jQuery('.fakecursor__next').remove();
				  }, 1000);
				}
				if (args.kind === 'clock') {
					jQuery('body').append('<div class="fakecursor__clock"><div class="fakecursor__clock__click"></div></div>');
					jQuery('.fakecursor__clock__click').show();
					setTimeout(function()
				  {
						jQuery('.clock__menu').show();
						jQuery('.fakecursor__clock').remove();
					}, 2000);
				}
				if (args.kind === 'whoiswho') {
					jQuery('body').append('<div class="fakecursor__whoiswho"><div class="fakecursor__whoiswho__click"></div></div>');
					jQuery('.fakecursor__whoiswho__click').show();
					setTimeout(function()
				  {
						jQuery.slideOver({'category': 'whoiswho', 'name': 'benjamin', 'extension': 'php'});
						jQuery('.fakecursor__whoiswho').remove();
					}, 800);
				}
		 },
		};

		function initialize() {
            _private.setup();
        }
        initialize();
		return this;
    };
}( jQuery ));

// Put on a slide
// jQuery.fakeCursor({'kind': 'next'});

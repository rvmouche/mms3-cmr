(function( $ ) {
	$.slideOver = function(options) {
    var args	 							= $.extend({}, options );

		var _this 							= this;

		var _private = {
			holder              	: _this,
			click									: null,


			setup: function() {
				_private.getSlideOver();
			},

			getSlideOver: function() {
				jQuery(".slideover").remove();
				$.ajax({
				 url: 'assets/' + args.category + '-' + args.name + '.' + args.extension,
			 })
			 .done(function( data ) {
				 jQuery('body').append(data);
				 jQuery(".slideover--right").animate({right: 0}, 300);
				 jQuery(".slideover--left").animate({left: 0}, 300);

				 jQuery('.slideover__header__close').on('click', function(event) {
  					 _private.clickListener('close');
  			 });
			 });
		 },
		 clickListener: function() {
			 jQuery(".slideover--right").animate({right: '-30vw'}, 300);
			 jQuery(".slideover--left").animate({left: '-30vw'}, 300);
			 jQuery(".slideover video").each(function(index, el) {
			 		jQuery.nativePlay({'id': jQuery(this).attr('id'), 'action': 'pause'});
			 });
		 }
		};

		function initialize() {
            _private.setup();
        }
        initialize();
		return this;
    };
}( jQuery ));

// Put on a slide
// jQuery.slideOver({'category': 'whoiswho', 'name': 'benjamin', 'extension': 'php'});

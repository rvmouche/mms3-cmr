(function( $ ) {
	$.localClock = function(options) {
    var args	 							= $.extend({}, options );

		var _this 							= this;

		var _private = {
			holder              	: _this,
			click									: null,


			setup: function() {
					_private.getClock();
			},
			getClock: function() {
				if (jQuery(".clock")[0]){
						_private.clock = jQuery('.clock');
				    _private.fillClock();
				} else {
						$.ajax({
						 url: 'parts/clock.php',
					 })
					 .done(function( data ) {
						 jQuery('body').append(data);
						 _private.clock = jQuery('.clock');
						 _private.fillClock();
					 });
				}
			},
			fillClock: function() {
				if (args.time) {
					jQuery(_private.clock).find('.clock__time').text(args.time);
				}
				if (args.video) {
					jQuery(_private.clock).addClass('clock--video');
				} else {
					jQuery(_private.clock).removeClass('clock--video');
				}
			}
		};

		function initialize() {
            _private.setup();
        }
        initialize();
		return this;
    };
}( jQuery ));

// Start the clock
jQuery.localClock({'time': jQuery('.introduction__section').attr('data-time-start'), 'video': true});

jQuery.extend({
  clockMenu: function(holder) {
    // PUBLIC
    var _this = this;
    var _private = {
      // PRIVATE
      holder: _this,
      /**
       * Setup
       * Setting up variables and binding events
       */
      setup: function() {
        _private.holder = holder;
        _private.menu = jQuery(_private.holder).find('.clock__menu');

        _private.createMenu();

        jQuery(_private.holder).on('click', function(event) {
            _private.clickListener('showMenu', null);
        });

        jQuery(_private.menu).on('click', function(event) {
            _private.clickListener('menuClick', event);
        });
      },
      clickListener: function(kind, event) {
        jQuery(".slideover").remove();

        if (kind === 'showMenu') {
          jQuery(_private.menu).toggle();
        }

        if (kind === 'menuClick') {
          var scroll = jQuery(event.target).attr('data-link');
          jQuery('html, body').animate({
              scrollTop: jQuery(scroll).offset().top
          }, 500);
          _private.setTime(jQuery(event.target).attr('data-link'));
        }
      },
      createMenu: function() {
        jQuery('section').each(function (index, value) {
          var link = jQuery(this).attr('data-menu');
          if (link === "introduction") {
            link = "." + link + "__section";
          } else {
            link = "." + link + "__nav";
          }
          jQuery(_private.menu).append('<a class="clock__menu__item" data-link="' + link + '">' + jQuery(this).attr('data-menu-name') + '</a>');
        });
      },
      setTime: function(element) {
        var clockNav = element;
        var takeOut = "";
        if (clockNav != '.introduction__section') {
          takeOut = '__nav';
          clockNav = clockNav.replace(takeOut,'') + '__section';
        }
        jQuery('.clock__time').text(jQuery(clockNav).attr('data-time-start'));
      }
    };
    /**
     * Initializing Plugin
     * Initializing the pseudo-object-oriented way of writing jQuery plugins
     * See function _private.setup for the next step
     */
    function initialize() {
      _private.setup();
    }
    initialize();
  }
});
/**
 * Starting the jQuery function
 * Here we register everything and start the plugin;
 * See function initialize for the next step
 */
jQuery(function() {
  jQuery(document).ready(function() {
    jQuery('.clock').each(function(_index, _ell) {
      var clockMenu = new jQuery.clockMenu(jQuery(_ell));
    });
  });
});

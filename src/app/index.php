<?php
  // HTML HEAD
  include 'parts/head.php';

  // Sections
  include 'parts/introduction.php';
  include 'parts/definition.php';
  include 'parts/mr.php';
  include 'parts/agenda.php';
  include 'parts/whoiswho.php';
  include 'parts/closing.php';

  // HTML Footer
  include 'parts/footer.php';
?>

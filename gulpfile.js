// Dependencies declaration
var gulp       = require('gulp'),
  del          = require('del'),
  sass         = require('gulp-sass'),
  uglify       = require('gulp-uglify'),
  gutil        = require('gulp-util'),
  jshint       = require('gulp-jshint'),
  stylish      = require('jshint-stylish'),
  concat       = require('gulp-concat'),
  svgmin       = require('gulp-svgmin'),
  autoprefixer = require('gulp-autoprefixer'),
  plumber      = require('gulp-plumber'),
  postcss      = require('gulp-postcss'),
  rename       = require('gulp-rename'),
  runTimestamp = Math.round(Date.now() / 1000);

var path = {
  js: {
    src: [
      './src/_base/vendor/js/jquery.js',
      './src/_base/vendor/js/**.js',
      './src/_base/utils/js/global.js',
      './src/_base/utils/js/**.js',
      './src/_structure/**/**/js/**.js'
    ],
    dist: './dist/js/'
  },
  css: {
    src: [
      './src/_base/utils/sass/**.scss',
      './src/_base/library/sass/**.scss',
      './src/_structure/**/**/sass/**.scss'
    ],
    dist: './dist/css/'
  },
  img: {
    root: {
      svg: {
        src: [
          './src/_base/utils/assets/**.svg',
          './src/_base/library/assets/**.svg',
          './src/_structure/**/**/assets/**.svg',
          './src/app/img/*.svg',
        ],
        dist: './dist/assets/'
      },
      png: {
        src: [
          './src/_base/utils/assets/**.png',
          './src/_base/library/assets/**.png',
          './src/_structure/**/**/assets/**.png',
          './src/app/img/*.png',
          './src/app/*.png',
        ],
        dist: './dist/assets/'
      },
      jpg: {
        src: [
          './src/_base/utils/assets/**.jpg',
          './src/_base/library/assets/**.jpg',
          './src/_structure/**/**/assets/**.jpg',
          './src/app/img/*.jpg',
          './src/app/img/*.jpeg',
        ],
        dist: './dist/assets/'
      }
    }
  },
  font: {
    src: [
      './src/_base/vendor/font/**/*.svg',
      './src/_base/vendor/font/**/*.ttf',
      './src/_base/vendor/font/**/*.eot',
      './src/_base/vendor/font/**/*.woff',
      './src/_base/vendor/font/**/*.woff2',
      './src/_base/font/**/*.svg',
      './src/_base/font/**/*.ttf',
      './src/_base/font/**/*.eot',
      './src/_base/font/**/*.woff',
      './src/_base/font/**/*.woff2',
    ],
    dist: './dist/fonts/'
  },
  php: {
    main: {
      src: [
        './src/app/**.php',
      ],
      dist: './dist/'
    },
    parts: {
      src: [
        './src/_structure/**/**/php/**.php'
      ],
      dist: './dist/parts/'
    },
    assets: {
      src: [
        './src/_structure/**/**/assets/**.php'
      ],
      dist: './dist/assets/'
    }
  }
};

gulp.task('process-sass', function() {
  gulp.src(path.css.src)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: [
        'last 3 versions',
        '> 2%',
        'ie 9',
        'ios 6',
        'android 4'
      ]
    }))
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'compressed'
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.css.dist));
});

gulp.task('process-js', function() {
  gulp.src(path.js.src)
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(plumber.stop())
    .pipe(gulp.dest(path.js.dist));
});

gulp.task('process-bitmap', function() {
  gulp.src(path.img.root.png.src)
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.img.root.png.dist));

  gulp.src(path.img.root.jpg.src)
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.img.root.jpg.dist));
});

gulp.task('process-svg', function() {
  gulp.src(path.img.root.svg.src)
    .pipe(svgmin({
      plugins: [{
        cleanupIDs: false
      }]
    }))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.img.root.svg.dist));
});

gulp.task('process-font', function() {
  gulp.src(path.font.src)
    .pipe(gulp.dest(path.font.dist));
});

gulp.task('process-php', function() {
  gulp.src(path.php.main.src)
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.php.main.dist));

  gulp.src(path.php.parts.src)
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(path.php.parts.dist));

    gulp.src(path.php.assets.src)
      .pipe(rename({
        dirname: ''
      }))
      .pipe(gulp.dest(path.php.assets.dist));
});

gulp.task('cleandist', function() {
  del('./dist');
});

gulp.task('default', function() {
  gulp.watch(path.css.src, ['process-sass']);
  gulp.watch(path.js.src, ['process-js']);
  gulp.watch(path.img.root.png.src, ['process-bitmap']);
  gulp.watch(path.img.root.jpg.src, ['process-bitmap']);
  gulp.watch(path.img.root.svg.src, ['process-svg']);
  gulp.watch(path.font.src, ['process-font']);
  gulp.watch(path.php.main.src, ['process-php']);
  gulp.watch(path.php.parts.src, ['process-php']);
  gulp.watch(path.php.assets.src, ['process-php']);
});

gulp.task('build', [
  'process-sass',
  'process-bitmap',
  'process-svg',
  'process-js',
  'process-font',
  'process-php'
]);
